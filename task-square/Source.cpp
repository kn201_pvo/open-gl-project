#include <iostream>

// GLEW
#define GLEW_STATIC
#include <GL/glew.h>

// GLFW
#include <GLFW/glfw3.h>


// �������� ������� ����������� �������
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);

// ������ ����
const GLuint WIDTH = 800, HEIGHT = 600;

// �������
const GLchar* vertexShaderSource = "#version 330 core\n"
"layout (location = 0) in vec3 position;\n"
"void main()\n"
"{\n"
"gl_Position = vec4(position.x, position.y, position.z, 1.0);\n"
"}\0";
const GLchar* fragmentShaderSource = "#version 330 core\n"
"out vec4 color;\n"
"void main()\n"
"{\n"
"color = vec4(1.0f, 0.5f, 0.2f, 1.0f);\n"
"}\n\0";

int main()
{
    std::cout << "Starting GLFW context, OpenGL 3.3" << std::endl;
    // ������������ GLFW
    glfwInit();
    // ������������ GLFW, �������� ��������� ����� OpenGL
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); // �������
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3); // ̳�����
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); // ������������ ��������, ��� ����� ����������� ��������
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE); // ��������� ������ ����� ����

    // ��������� ��'���� ����
    GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "LearnOpenGL", nullptr, nullptr);
    glfwMakeContextCurrent(window);

    // �������� ������� � GLFW
    glfwSetKeyCallback(window, key_callback);

    // ��� ������� ��� ��������� ������������ OpenGL
    glewExperimental = GL_TRUE;
    // ������������ GLEW
    glewInit();

    // ������ ������ ����
    int width, height;
    glfwGetFramebufferSize(window, &width, &height);
    glViewport(0, 0, width, height); // 1 � 2 - ���������� ����� �������� ���� ����


    // ��������� ������
    GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertexShader, 1, &vertexShaderSource, NULL); //��'����� ��� ������� � ��'����� � ���������
    glCompileShader(vertexShader);
    // ���������� �� �������
    GLint success;
    GLchar infoLog[512];
    glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        // �����, ���� ���������� � ��� �������
        glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
    }
    // ����������� ������
    GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
    glCompileShader(fragmentShader);
    glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
    }
    // ��������� �������� ��������
    GLuint shaderProgram = glCreateProgram();
    // �������� �������
    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);
    // ��'����� ��
    glLinkProgram(shaderProgram);
    // ��������
    glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
    if (!success) {
        glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
    }
    // ��������� �������
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);


    // ������� ������������
    GLfloat vertices[] = {
         0.5f,  0.5f, 0.0f,  // Top Right
         0.5f, -0.5f, 0.0f,  // Bottom Right
        -0.5f, -0.5f, 0.0f,  // Bottom Left
        -0.5f,  0.5f, 0.0f   // Top Left 
    };
    GLuint indices[] = {  // Note that we start from 0!  ������� ������ ����������
        0, 1, 3,  // First Triangle
        1, 2, 3   // Second Triangle
    };
    // ��������� ��'���� ���������� ������, ������ ������, ������ �������
    GLuint VBO, VAO, EBO;
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glGenBuffers(1, &EBO); // ����� � ���������
    // ����'����� VAO
    glBindVertexArray(VAO);
    // ������� ����� � ��������� � �����
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    // ������� ������� � �����
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
    //�������������, ������������ ��������� �� �������� ��������
    // 1 - �������� ������� ��� ������������, 2 - ����� ���, 3 - ��� �����, 4 - ������������ ������������ ����, 5 - ����, 6 - ������� ������� ����� � �����
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
    glEnableVertexAttribArray(0);

    glBindBuffer(GL_ARRAY_BUFFER, 0); 

    glBindVertexArray(0); // ���'����� ��'��� ������ � ���������



    // ������� ����
    while (!glfwWindowShouldClose(window))
    {
        // �������� ���� � ������ ������� ����������� �������
        glfwPollEvents();

        // Render
        // Clear the colorbuffer
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        // ������� ������ ���������
        glUseProgram(shaderProgram);
        glBindVertexArray(VAO);
        // ������ ���������
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
        glBindVertexArray(0);

        // ������ ������ ������
        glfwSwapBuffers(window);
    }
    // ��������� ��'����
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
    glDeleteBuffers(1, &EBO);
    // �������� ���'��
    glfwTerminate();
    return 0;
}

// ������� ����������� �������, ����������� ��� ���������� �� ������
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
    // ���� ���������� ������� ESC, ������� ����������� 
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);
}