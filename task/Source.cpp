#include <iostream>

// GLEW
#define GLEW_STATIC
#include <GL/glew.h>

// GLFW
#include <GLFW/glfw3.h>

// ��������� ����� ������
#include "shader_s.h"

// �������� ������� ����������� �������
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);

// ������ ����
const GLuint WIDTH = 800, HEIGHT = 600;

int main()
{
    // ������������ GLFW
    glfwInit();
    // ������������ GLFW, �������� ��������� ����� OpenGL
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); // �������
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3); // ̳�����
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); // ������������ ��������, ��� ����� ����������� ��������
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE); // ��������� ������ ����� ����

    // ��������� ��'���� ����
    GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "LearnOpenGL", nullptr, nullptr);
    glfwMakeContextCurrent(window);

    // �������� ������� � GLFW
    glfwSetKeyCallback(window, key_callback);

    // ��� ������� ��� ��������� ������������ OpenGL
    glewExperimental = GL_TRUE;
    // ������������ GLEW
    glewInit();

    // ������ ������ ����
    int width, height;
    glfwGetFramebufferSize(window, &width, &height);
    glViewport(0, 0, width, height);

    // ��������� ��'���� �����,  �������� ����� � ��������� � ����������� ��������
    Shader ourShader("H:/course-2/opengl/lab2/task/vertex.vs", "H:/course-2/opengl/lab2/task/fragment.frag");
 
    // ������� ����������
    GLfloat vertices[] = {
        0.5f, -0.5f, 0.0f,  1.0f, 0.0f, 0.0f,   // ������ ������ ����
       -0.5f, -0.5f, 0.0f,  0.0f, 1.0f, 0.0f,   // ������ ����� ����
        0.0f,  0.5f, 0.0f,  0.0f, 0.0f, 1.0f    // ������� ����
    };
    // ��������� ��'���� ���������� ������, ������ ������
    GLuint VBO, VAO;
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    // ����'����� VAO
    glBindVertexArray(VAO);
    // ������� ����� � ��������� � �����
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    //�������������, ������������ ��������� �� �������� ��������
    // 1 - �������� ������� ��� ������������, 2 - ����� ���, 3 - ��� �����, 4 - ������������ ������������ ����, 5 - ����, 6 - ������� ������� ����� � �����
   // ������� � ������������
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)0);
    glEnableVertexAttribArray(0);
    // ������� � ������
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
    glEnableVertexAttribArray(1);

    glBindBuffer(GL_ARRAY_BUFFER, 0); 

    glBindVertexArray(0); // ���'����� ��'��� ������ � ���������

    // ������� ����
    while (!glfwWindowShouldClose(window))
    {
        /// �������� ���� � ������ ������� ����������� �������
        glfwPollEvents();

        // Render
        // Clear the colorbuffer
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        // ������� ���������
        ourShader.Use();

        GLfloat timeValue = glfwGetTime();
        GLfloat greenValue = (sin(timeValue) / 2) + 0.5;
        GLint vertexColorLocation = glGetUniformLocation(ourShader.Program, "ourColor");
        ourShader.Use();
        glUniform4f(vertexColorLocation, 0.0f, greenValue, 0.0f, 1.0f);

        glBindVertexArray(VAO);
        glDrawArrays(GL_TRIANGLES, 0, 3);
        glBindVertexArray(0);

        // ������ ������ ������
        glfwSwapBuffers(window);
    }
    // ��������� ��'����
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
    // �������� ���'��
    glfwTerminate();
    return 0;
}

// ������� ����������� �������, ����������� ��� ���������� �� ������
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
    // ���� ���������� ������� ESC, ������� �����������
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);
}